class Board
  attr_accessor :grid

  def initialize(grid = nil)
    grid == nil ? @grid = Array.new(3) { Array.new(3) }: @grid = grid
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    return true if grid[pos[0]][pos[1]] == nil
  end

  def winner
    grid.each do |row|
      return :X if row.count(:X) == 3
      return :O if row.count(:O) == 3
      return :X if (grid[0][0] == :X && grid[1][1] == :X && grid[2][2] == :X)
      return :O if (grid[0][0] == :O && grid[1][1] == :O && grid[2][2] == :O)
      return :X if (grid[0][2] == :X && grid[1][1] == :X && grid[2][0] == :X)
      return :O if (grid[0][2] == :O && grid[1][1] == :O && grid[2][0] == :O)
    end
    grid.transpose.each do |col|
      return :X if col.count(:X) == 3
      return :O if col.count(:O) == 3
    end
    nil
  end

  def over?
    return true if winner
    counter = 0
    grid.each do |row|
      counter += 1 if row.include?(nil)
    end
    return true if counter == 0
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

end
