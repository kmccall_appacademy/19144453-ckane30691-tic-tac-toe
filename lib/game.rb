require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :player1, :player2, :board, :current_player

  def initialize(player1 = HumanPlayer.new, player2 = ComputerPlayer.new)
    @player1 = player1
    @player2 = player2
    @board = Board.new
    @current_player = player1
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def switch_players!
    self.current_player = current_player == player1 ? player2 : player1
  end

  def play
    current_player.display(board)
    until board.over?
      play_turn
    end
    if game_winner
      game_winner.display(board)
      puts "#{game_winner.name} wins!"
    else
      puts "Cat's game"
    end
  end

  def game_winner
    return player1 if board.winner == player1.mark
    return player2 if board.winner == player2.mark
    nil
  end

end

if __FILE__ == $PROGRAM_NAME
  print "Enter your name: "
  name = gets.chomp.strip
  human = HumanPlayer.new(name)
  bill = ComputerPlayer.new('Bill')

  new_game = Game.new(human, bill)
  new_game.play
end