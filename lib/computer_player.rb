class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name, mark = :O)
    @name = name
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    if winning_move
      winning_move
    else
      moves = []
      (0..2).each do |row|
        (0..2).each do |col|
          moves << [row, col] if board.grid[row][col] == nil
        end
      end
      return moves[rand(0...moves.length)]
    end
  end

  def winning_move
    board.grid.each do |row| 
      if row.count(mark) == 2 && row.include?(nil)
        return board.grid.index(row), row.index(nil)
      end
    end
    board.grid.transpose.each do |col|
      if col.count(mark) == 2 && col.include?(nil)
        return col.index(nil), board.grid.transpose.index(col)
      end
    end
    case
      #right diag logic
      when board.grid[0][0] == mark && board.grid[1][1] == mark
        return 2, 2 if board.grid[2][2] == nil
      when board.grid[0][0] == mark && board.grid[2][2] == mark
        return 1, 1 if board.grid[1][1] == nil
      when board.grid[1][1] == mark && board.grid[2][2] == mark
        return 0, 0 if board.grid[0][0] == nil
      #left diag logic
      when board.grid[0][2] == mark && board.grid[1][1] == mark 
        return 2, 0 if board.grid[2][0] == nil
      when board.grid[0][2] == mark && board.grid[2][0] == mark
        return 1, 1 if board.grid[1][1] == nil
      when board.grid[2][0] == mark && board.grid[1][1] == mark
        return 0, 2 if board.grid[0][2] == nil
    end
    false
  end



end
